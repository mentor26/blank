package com.homework;

public abstract class Velocity {

    public abstract void drive();
    public abstract void stop();
}
