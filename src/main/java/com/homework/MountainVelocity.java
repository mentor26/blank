package com.homework;

public class MountainVelocity extends Velocity {

    private Long wheelCount;

    public void drive() {
        this.drive();
    }

    public void drive(Long speed) {
        this.drive(speed);
    }

    @Override
    public void stop() {
    }

    public Long getWheelCount() {
        return wheelCount;
    }

    public void setWheelCount(Long wheelCount) {
        this.wheelCount = wheelCount;
    }
}
