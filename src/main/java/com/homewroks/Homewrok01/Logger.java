package com.homewroks.Homewrok01;

public class Logger {

    private static final Logger logger;

    static {
        logger = new Logger();
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void log(String message) {
        System.out.println(message);
    }
}
