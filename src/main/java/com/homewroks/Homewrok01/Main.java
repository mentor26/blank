package com.homewroks.Homewrok01;

class Main {
    static class Parent {

        private static int age = 5;

        {
            System.out.println("Parent nonstatic block");
        }

        static {
            System.out.println("Parent static block");
        }

        Parent() {
            System.out.println("Parent constructor");
        }

        public static int getAge() {
            return age;
        }
    }

    static class Child
            extends Parent {

        private static int age = 10;

        {
            System.out.println("Child nonstatic block");
        }

        static {
            System.out.println("Child static block");
        }

        Child() {
            System.out.println("Child constructor");
        }

        public static int getAge() {
            return age;
        }
    }

    public static void main(String[] args) {
        Child child = new Child();
        System.out.println(child.getAge());
    }
}

